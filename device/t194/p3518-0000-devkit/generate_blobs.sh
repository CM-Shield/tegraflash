#!/bin/bash

PATH=${PWD}/../../../tools/nvblob:${PWD}/../../../tools/tegraflash:${PATH}

export ANDROID_BUILD_TOP=..
export OUT=${PWD}

python2 ${PWD}/../../../tools/nvblob/BUP_generator.py -t bmp -e \
  "../../../bmp/nvidia1080.bmp nvidia 1080; \
   ../../../bmp/verity_orange_continue_1080.bmp verity_orange_continue 1080; \
   ../../../bmp/verity_orange_pause_1080.bmp verity_orange_pause 1080; \
   ../../../bmp/verity_red_continue_1080.bmp verity_red_continue 1080; \
   ../../../bmp/verity_red_pause_1080.bmp verity_red_pause 1080; \
   ../../../bmp/verity_yellow_continue_1080.bmp verity_yellow_continue 1080; \
   ../../../bmp/verity_yellow_pause_1080.bmp verity_yellow_pause 1080"

mkdir blob_signed
pushd blob_signed

ln -s ../*.bin .
ln -s ../*.img .
ln -s ../*.dtb .
ln -s ../*.cfg .
ln -s ../flash_android_t194_spi_sd_p3668.xml flash_android_t194_spi_sd_p3668.xml.tmp

tegrasign_v3.py --getmontgomeryvalues montgomery.bin --key None --list rcm_list.xml --pubkeyhash pub_key.key
tegraparser_v2 --pt flash_android_t194_spi_sd_p3668.xml.tmp
tegrahost_v2 --chip 0x19 0 --partitionlayout flash_android_t194_spi_sd_p3668.xml.bin --list images_list.xml zerosbk
tegrasign_v3.py --key None --list images_list.xml --pubkeyhash pub_key.key

sw_memcfg_overlay.pl -c tegra194-mb1-bct-memcfg-p3668-0001-a00.cfg -s tegra194-memcfg-sw-override.cfg -o memcfg.cfg
tegrabct_v2 --dev_param tegra194-br-bct-qspi.cfg --sdram memcfg.cfg --brbct br_bct.cfg --sfuse tegra194-mb1-soft-fuses-l4t.cfg --chip 0x19 0
tegrabct_v2 --brbct br_bct_BR.bct --chip 0x19 0 --updateblinfo flash_android_t194_spi_sd_p3668.xml.bin --updatesig images_list_signed.xml
tegrabct_v2 --brbct br_bct_BR.bct --chip 0x19 --updatesmdinfo flash_android_t194_spi_sd_p3668.xml.bin
tegrabct_v2 --brbct br_bct_BR.bct --chip 0x19 0 --updatefields "Odmdata =0xB8190000"
tegrabct_v2 --brbct br_bct_BR.bct --chip 0x19 0 --listbct bct_list.xml
tegrasign_v3.py --key None --list bct_list.xml --pubkeyhash pub_key.key --getmontgomeryvalues montgomery.bin
tegrabct_v2 --brbct br_bct_BR.bct --chip 0x19 0 --updatesig bct_list_signed.xml

tegrabct_v2 --chip 0x19 0 \
  --mb1bct mb1_cold_boot_bct.cfg \
  --sdram memcfg.cfg \
  --misc tegra194-mb1-bct-misc-l4t.cfg \
  --scr tegra194-mb1-bct-scr-cbb-mini-p3668.cfg \
  --pinmux tegra19x-mb1-pinmux-p3668-a01.cfg \
  --pmc tegra19x-mb1-padvoltage-p3668-a01.cfg \
  --pmic tegra194-mb1-bct-pmic-p3668-0001-a00.cfg \
  --brcommand tegra194-mb1-bct-reset-p3668-0001-a00.cfg \
  --prod tegra19x-mb1-prod-p3668-0001-a00.cfg \
  --gpioint tegra194-mb1-bct-gpioint-p3668-0001-a00.cfg \
  --device tegra19x-mb1-bct-device-qspi-p3668.cfg

tegrabct_v2 --chip 0x19 --mb1bct mb1_cold_boot_bct_MB1.bct --updatefwinfo flash_android_t194_spi_sd_p3668.xml.bin
tegrabct_v2 --chip 0x19 --mb1bct mb1_cold_boot_bct_MB1.bct --updatestorageinfo flash_android_t194_spi_sd_p3668.xml.bin
tegraflash.py --chip 0x19 --cmd "sign mb1_cold_boot_bct_MB1.bct"

tegrabct_v2 --chip 0x19 0 \
  --sdram memcfg.cfg \
  --membct memcfg_1.bct memcfg_2.bct memcfg_3.bct memcfg_4.bct

tegrahost_v2 --chip 0x19 0 --blocksize 512 --magicid MEMB --addsigheader_multi memcfg_1.bct memcfg_2.bct memcfg_3.bct memcfg_4.bct
mv memcfg_1_sigheader.bct mem_coldboot.bct
tegrahost_v2 --chip 0x19 --align mem_coldboot.bct
tegrahost_v2 --chip 0x19 0 --magicid MEMB --appendsigheader mem_coldboot.bct zerosbk
tegrabct_v2 --brbct mem_coldboot_sigheader.bct --chip 0x18 0 --listbct mem_coldboot_sigheader.bct_list.xml
tegrasign_v3.py --key None --list mem_coldboot_sigheader.bct_list.xml --pubkeyhash pub_key.key
tegrahost_v2 --chip 0x19 0 --updatesigheader mem_coldboot_sigheader.bct.encrypt mem_coldboot_sigheader.bct.hash zerosbk

tegrahost_v2 --chip 0x19 0 --partitionlayout flash_android_t194_spi_sd_p3668.xml.bin --updatesig images_list_signed.xml

popd

mkdir blob_signed_emmc
pushd blob_signed_emmc

ln -s ../*.cfg .
ln -s ../flash_android_t194_spi_emmc_p3668.xml flash_android_t194_spi_emmc_p3668.xml.tmp

tegrasign_v3.py --getmontgomeryvalues montgomery.bin --key None --list rcm_list.xml --pubkeyhash pub_key.key
tegraparser_v2 --pt flash_android_t194_spi_emmc_p3668.xml.tmp
tegrahost_v2 --chip 0x19 0 --partitionlayout flash_android_t194_spi_emmc_p3668.xml.bin --list images_list.xml zerosbk
tegrasign_v3.py --key None --list images_list.xml --pubkeyhash pub_key.key

sw_memcfg_overlay.pl -c tegra194-mb1-bct-memcfg-p3668-0001-a00.cfg -s tegra194-memcfg-sw-override.cfg -o memcfg.cfg

tegrabct_v2 --chip 0x19 0 \
  --mb1bct mb1_cold_boot_bct.cfg \
  --sdram memcfg.cfg \
  --misc tegra194-mb1-bct-misc-l4t.cfg \
  --scr tegra194-mb1-bct-scr-cbb-mini-p3668.cfg \
  --pinmux tegra19x-mb1-pinmux-p3668-a01.cfg \
  --pmc tegra19x-mb1-padvoltage-p3668-a01.cfg \
  --pmic tegra194-mb1-bct-pmic-p3668-0001-a00.cfg \
  --brcommand tegra194-mb1-bct-reset-p3668-0001-a00.cfg \
  --prod tegra19x-mb1-prod-p3668-0001-a00.cfg \
  --gpioint tegra194-mb1-bct-gpioint-p3668-0001-a00.cfg \
  --device tegra19x-mb1-bct-device-qspi-p3668.cfg

tegrabct_v2 --chip 0x19 --mb1bct mb1_cold_boot_bct_MB1.bct --updatefwinfo flash_android_t194_spi_emmc_p3668.xml.bin
tegrabct_v2 --chip 0x19 --mb1bct mb1_cold_boot_bct_MB1.bct --updatestorageinfo flash_android_t194_spi_emmc_p3668.xml.bin
tegraflash.py --chip 0x19 --cmd "sign mb1_cold_boot_bct_MB1.bct"

popd

python2 ${PWD}/../../../tools/nvblob/BUP_generator.py -t update -e \
  "blob_signed/spe_t194_sigheader.bin.encrypt spe-fw 2 0 common; \
   blob_signed/nvtboot_t194_sigheader.bin.encrypt mb2 2 0 common; \
   blob_signed/cboot_t194_sigheader.bin.encrypt cpu-bootloader 2 0 common; \
   blob_signed/tos_t194_sigheader.img.encrypt secure-os 2 0 common; \
   blob_signed/bpmp_t194_sigheader.bin.encrypt bpmp-fw 2 0 common; \
   adsp-fw.bin adsp-fw 2 0 common; \
   blob_signed/camera-rtcpu-rce_sigheader.img.encrypt rce-fw 2 0 common; \
   blob_signed/preboot_c10_prod_cr_sigheader.bin.encrypt mts-preboot 2 2 common; \
   blob_signed/mce_c10_prod_cr_sigheader.bin.encrypt mts-mce 2 2 common; \
   blob_signed/mts_c10_prod_cr_sigheader.bin.encrypt mts-proper 2 2 common; \
   blob_signed/warmboot_t194_prod_sigheader.bin.encrypt sc7 2 2 common; \
   blob_signed/mb1_t194_prod_sigheader.bin.encrypt mb1 2 2 common; \
   blob_signed/tegra194-a02-bpmp-p3668-a00_sigheader.dtb.encrypt bpmp-fw-dtb 2 0 P3518-0000-DEVKIT.default; \
   blob_signed/tegra194-a02-bpmp-p3668-a00_sigheader.dtb.encrypt bpmp-fw-dtb 2 0 P3518-0001-DEVKIT.default; \
   blob_signed/tegra194-p3668-all-p3509-0000-android_sigheader.dtb.encrypt bootloader-dtb 2 0 P3518-0000-DEVKIT.default; \
   blob_signed/tegra194-p3668-all-p3509-0000-android_sigheader.dtb.encrypt bootloader-dtb 2 0 P3518-0001-DEVKIT.default; \
   blob_signed/br_bct_BR.bct BCT 2 2 P3518-0000-DEVKIT.default; \
   blob_signed/br_bct_BR.bct BCT 2 2 P3518-0001-DEVKIT.default; \
   blob_signed/mb1_cold_boot_bct_MB1_sigheader.bct.encrypt MB1_BCT 2 0 P3518-0000-DEVKIT.default; \
   blob_signed_emmc/mb1_cold_boot_bct_MB1_sigheader.bct.encrypt MB1_BCT 2 0 P3518-0001-DEVKIT.default; \
   blob_signed/mem_coldboot_sigheader.bct.encrypt MEM_BCT 2 0 P3518-0000-DEVKIT.default; \
   blob_signed/mem_coldboot_sigheader.bct.encrypt MEM_BCT 2 0 P3518-0001-DEVKIT.default; \
   blob_signed/tegra194-p3668-all-p3509-0000-android_sigheader.dtb.encrypt kernel-dtb 2 0 P3518-0000-DEVKIT.default; \
   blob_signed/tegra194-p3668-all-p3509-0000-android_sigheader.dtb.encrypt kernel-dtb 2 0 P3518-0001-DEVKIT.default"

mv ota.blob bl_update_payload

rm -rf blob_signed blob_signed_emmc

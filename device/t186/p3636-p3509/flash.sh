#!/bin/sh

PATH=$(pwd)/../../../tools/tegraflash:${PATH}

TARGET_TEGRA_VERSION=t186;
TARGET_MODULE_ID=3636;
TARGET_CARRIER_ID=3509;

source $(pwd)/../../../tools/scripts/helpers.sh;

declare -a FLASH_CMD_BASIC=(
  --applet mb1_recovery_prod.bin
  --chip 0x18);

if ! get_interfaces; then
  exit -1;
fi;

if ! check_module_compatibility ${TARGET_MODULE_ID}; then
  echo "No Jetson TX2 NX module found";
  exit -1;
fi;

declare -a FLASH_CMD_FULL=(
  ${FLASH_CMD_BASIC[@]}
  --bl nvtboot_recovery_cpu.bin
  --sdram_config tegra186-mb1-bct-memcfg-p3636-0001-a01.cfg
  --odmdata 0x2090000
  --misc_config tegra186-mb1-bct-misc-si-l4t.cfg
  --pinmux_config tegra186-mb1-bct-pinmux-p3636-0001-a00.cfg
  --pmic_config tegra186-mb1-bct-pmic-p3636-0001-a00.cfg
  --pmc_config tegra186-mb1-bct-pad-p3636-0001-a00.cfg
  --prod_config tegra186-mb1-bct-prod-p3636-0001-a00.cfg
  --scr_config minimal_scr.cfg
  --scr_cold_boot_config mobile_scr.cfg
  --br_cmd_config tegra186-mb1-bct-bootrom-p3636-0001-a00.cfg
  --dev_params emmc.cfg
  --bins "mb2_bootloader nvtboot_recovery.bin; mts_preboot preboot_d15_prod_cr.bin; mts_bootpack mce_mts_d15_prod_cr.bin; bpmp_fw bpmp.bin; bpmp_fw_dtb tegra186-bpmp-p3636-0001-a00-00.dtb; tlk tos.img; eks eks.img; bootloader_dtb tegra186-p3636-0001-p3509-0000-a01-android.dtb");

if ! check_carrier_compatibility ${TARGET_CARRIER_ID}; then
  echo "No NX Devkit Carrier with Jetson TX2 NX found";
  exit -1;
fi;

tegraflash.py \
  "${FLASH_CMD_FULL[@]}" \
  --instance ${INTERFACE} \
  --cfg flash_android_t186.xml \
  --cmd "flash; reboot"

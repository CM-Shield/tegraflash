#!/bin/bash

PATH=${PWD}/../../../tools/nvblob:${PWD}/../../../tools/tegraflash:${PATH}

export ANDROID_BUILD_TOP=..
export OUT=${PWD}

python2 ${PWD}/../../../tools/nvblob/BUP_generator.py -t bmp -e \
  "../../../bmp/nvidia1080.bmp nvidia 1080; \
   ../../../bmp/verity_orange_continue_1080.bmp verity_orange_continue 1080; \
   ../../../bmp/verity_orange_pause_1080.bmp verity_orange_pause 1080; \
   ../../../bmp/verity_red_continue_1080.bmp verity_red_continue 1080; \
   ../../../bmp/verity_red_pause_1080.bmp verity_red_pause 1080; \
   ../../../bmp/verity_yellow_continue_1080.bmp verity_yellow_continue 1080; \
   ../../../bmp/verity_yellow_pause_1080.bmp verity_yellow_pause 1080"

mkdir blob_signed
pushd blob_signed

ln -s ../*.bin .
ln -s ../*.img .
ln -s ../*.dtb .
ln -s ../flash_android_t186.xml flash_android_t186.xml.tmp
ln -s tegra186-a02-bpmp-quill-p3310-1000-c04-00-te770d-ucm2.dtb tegra186-a02-bpmp-quill-p3310-1000.dtb

tegraparser_v2 --pt flash_android_t186.xml.tmp
tegrahost_v2 --chip 0x18 0 --partitionlayout flash_android_t186.xml.bin --list images_list.xml zerosbk
tegrasign_v3.py --key None --list images_list.xml --pubkeyhash pub_key.key

tegrabct_v2 --dev_param ../emmc.cfg --sdram ../P3310_A00_8GB_lpddr4_A02_l4t.cfg --brbct br_bct.cfg --chip 0x18 0
tegrabct_v2 --brbct br_bct_BR.bct --chip 0x18 0 --updateblinfo flash_android_t186.xml.bin --updatesig images_list_signed.xml
tegrabct_v2 --brbct br_bct_BR.bct --chip 0x18 --updatesmdinfo flash_android_t186.xml.bin
tegraparser_v2 --chip 0x18 --updatecustinfo br_bct_BR.bct
tegrabct_v2 --brbct br_bct_BR.bct --chip 0x18 0 --updatefields "Odmdata =0x1098000"
tegrabct_v2 --brbct br_bct_BR.bct --chip 0x18 0 --listbct bct_list.xml
tegrasign_v3.py --key None --list bct_list.xml --pubkeyhash pub_key.key
tegrabct_v2 --brbct br_bct_BR.bct --chip 0x18 0 --updatesig bct_list_signed.xml

tegrabct_v2 --chip 0x18 0 \
  --mb1bct mb1_cold_boot_bct.cfg \
  --sdram ../P3310_A00_8GB_lpddr4_A02_l4t.cfg \
  --misc ../tegra186-mb1-bct-misc-si-l4t.cfg \
  --scr ../mobile_scr.cfg \
  --pinmux ../tegra186-mb1-bct-pinmux-quill-p3310-1000-c03.cfg \
  --pmc ../tegra186-mb1-bct-pad-quill-p3310-1000-c03.cfg \
  --pmic ../tegra186-mb1-bct-pmic-quill-p3310-1000-c04.cfg \
  --brcommand ../tegra186-mb1-bct-bootrom-quill-p3310-1000-c03.cfg \
  --prod ../tegra186-mb1-bct-prod-quill-p3310-1000-c03.cfg

tegrabct_v2 --chip 0x18 --mb1bct mb1_cold_boot_bct_MB1.bct --updatefwinfo flash_android_t186.xml.bin
tegrabct_v2 --chip 0x18 --mb1bct mb1_cold_boot_bct_MB1.bct --updatestorageinfo flash_android_t186.xml.bin
tegraflash.py --chip 0x18 --cmd "sign mb1_cold_boot_bct_MB1.bct"

tegrahost_v2 --chip 0x18 0 --partitionlayout flash_android_t186.xml.bin --updatesig images_list_signed.xml

popd

mkdir blob_signed_c03
pushd blob_signed_c03

ln -s ../*.bin .
ln -s ../*.img .
ln -s ../*.dtb .
ln -s ../flash_android_t186.xml flash_android_t186.xml.tmp
ln -s tegra186-a02-bpmp-quill-p3310-1000-c01-00-te770d-ucm2.dtb tegra186-a02-bpmp-quill-p3310-1000.dtb

tegraparser_v2 --pt flash_android_t186.xml.tmp
tegrahost_v2 --chip 0x18 0 --partitionlayout flash_android_t186.xml.bin --list images_list.xml zerosbk
tegrasign_v3.py --key None --list images_list.xml --pubkeyhash pub_key.key

tegrabct_v2 --chip 0x18 0 \
  --mb1bct mb1_cold_boot_bct.cfg \
  --sdram ../P3310_A00_8GB_lpddr4_A02_l4t.cfg \
  --misc ../tegra186-mb1-bct-misc-si-l4t.cfg \
  --scr ../mobile_scr.cfg \
  --pinmux ../tegra186-mb1-bct-pinmux-quill-p3310-1000-c03.cfg \
  --pmc ../tegra186-mb1-bct-pad-quill-p3310-1000-c03.cfg \
  --pmic ../tegra186-mb1-bct-pmic-quill-p3310-1000-c03.cfg \
  --brcommand ../tegra186-mb1-bct-bootrom-quill-p3310-1000-c03.cfg \
  --prod ../tegra186-mb1-bct-prod-quill-p3310-1000-c03.cfg

tegrabct_v2 --chip 0x18 --mb1bct mb1_cold_boot_bct_MB1.bct --updatefwinfo flash_android_t186.xml.bin
tegrabct_v2 --chip 0x18 --mb1bct mb1_cold_boot_bct_MB1.bct --updatestorageinfo flash_android_t186.xml.bin
tegraflash.py --chip 0x18 --cmd "sign mb1_cold_boot_bct_MB1.bct"
popd

python2 ${PWD}/../../../tools/nvblob/BUP_generator.py -t update -e \
  "blob_signed/spe_sigheader.bin.encrypt spe-fw 2 0 common; \
   blob_signed/nvtboot_sigheader.bin.encrypt mb2 2 0 common; \
   blob_signed/cboot_sigheader.bin.encrypt cpu-bootloader 2 0 common; \
   blob_signed/tos_sigheader.img.encrypt secure-os 2 0 common; \
   blob_signed/bpmp_sigheader.bin.encrypt bpmp-fw 2 0 common; \
   blob_signed/adsp-fw_sigheader.bin.encrypt adsp-fw 2 0 common; \
   blob_signed/camera-rtcpu-sce_sigheader.img.encrypt rce-fw 2 0 common; \
   blob_signed/preboot_d15_prod_cr_sigheader.bin.encrypt mts-preboot 2 2 common; \
   blob_signed/mce_mts_d15_prod_cr_sigheader.bin.encrypt mts-bootpack 2 2 common; \
   blob_signed/warmboot_wbheader.bin.encrypt sc7 2 2 common; \
   blob_signed/mb1_prod.bin.encrypt mb1 2 2 common; \
   blob_signed_c03/tegra186-a02-bpmp-quill-p3310-1000_sigheader.dtb.encrypt bpmp-fw-dtb 2 0 P2771-0000-DEVKIT-C03.default; \
   blob_signed/tegra186-quill-p3310-1000-c03-00-base_sigheader.dtb.encrypt bootloader-dtb 2 0 P2771-0000-DEVKIT-C03.default; \
   blob_signed/br_bct_BR.bct BCT 2 2 P2771-0000-DEVKIT-C03.default; \
   blob_signed_c03/mb1_cold_boot_bct_MB1_sigheader.bct.encrypt MB1_BCT 2 0 P2771-0000-DEVKIT-C03.default; \
   tegra186-quill-p3310-1000-c03-00-base.dtb kernel-dtb 2 0 P2771-0000-DEVKIT-C03.default; \
   blob_signed/tegra186-a02-bpmp-quill-p3310-1000_sigheader.dtb.encrypt bpmp-fw-dtb 2 0 P2771-0000-DEVKIT-C04.default; \
   blob_signed/tegra186-quill-p3310-1000-c03-00-base_sigheader.dtb.encrypt bootloader-dtb 2 0 P2771-0000-DEVKIT-C04.default; \
   blob_signed/br_bct_BR.bct BCT 2 2 P2771-0000-DEVKIT-C04.default; \
   blob_signed/mb1_cold_boot_bct_MB1_sigheader.bct.encrypt MB1_BCT 2 0 P2771-0000-DEVKIT-C04.default; \
   tegra186-quill-p3310-1000-c03-00-base.dtb kernel-dtb 2 0 P2771-0000-DEVKIT-C04.default"

mv ota.blob bl_update_payload

rm -rf blob_signed blob_signed_c03

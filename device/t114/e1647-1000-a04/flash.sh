#!/bin/sh

PATH=$(pwd)/../../../tools/nvflash3:${PATH}

nvflash \
  --bct E1611_Hynix_2GB_H5TC4G63AFR-RDA_792Mhz_r403_v2.cfg \
  --setbct \
  --configfile android_fastboot_dtb_emmc_full.cfg \
  --create \
  --bl nvflash_bootloader.bin \
  --odmdata 0x80098000 \
  --go

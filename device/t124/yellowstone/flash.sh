#!/bin/sh

PATH=$(pwd)/../../../tools/nvflash4:${PATH}

nvflash_legacy \
  --bct E1791_Elpida_4GB_edfa232a2ma_924MHz.cfg \
  --setbct \
  --configfile tn8_android_fastboot_nvtboot_dtb_emmc_full.cfg \
  --create \
  --bl bootloader.bin \
  --odmdata 0x12498008 \
  --nct NCT_yellowstone.txt \
  --go

#!/bin/sh

PATH=$(pwd)/../../../tools/nvflash4:${PATH}

nvflash \
  --bct PM375_Hynix_2GB_H5TC4G63AFR_RDA_924MHz.cfg \
  --setbct \
  --configfile tn8_android_fastboot_nvtboot_dtb_emmc_full.cfg \
  --create \
  --bl bootloader.bin \
  --odmdata 0x6209C000 \
  --nct NCT_jetson-tk1.txt \
  --go

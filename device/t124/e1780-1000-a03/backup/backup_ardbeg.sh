#!/bin/sh

PATH=$(pwd)/../../../../tools/nvflash4:${PATH}

nvflash --bl ../bootloader.bin --getpartitiontable part.txt
nvflash --resume --getbct --bct board.bct
nvflash --resume --read EBT ebt.bin
nvflash --resume --read NVC nvc.bin
nvflash --resume --read GP1 gp1.bin
nvflash --resume --read KEY key.bin
nvflash --resume --read DFI dfi.bin
nvflash --resume --read TOS tos.img
nvflash --resume --read EKS eks.bin
nvflash --resume --read FB  fb.bin
nvflash --resume --read WB0 wb0.bin
nvflash --resume --read NCT nct.bin
nvflash --resume --read SOS sos.img
nvflash --resume --read DTB dtb.dtb
nvflash --resume --read LNX lnx.img
nvflash --resume --read APP app.img
nvflash --resume --read CAC cac.img
nvflash --resume --read MSC msc.bin
nvflash --resume --read USP usp.bin
nvflash --resume --read MDA mda.bin
nvflash --resume --read FCT fct.img
nvflash --resume --read LBP lbp.bin
nvflash --resume --read CHG chg.bin
nvflash --resume --read FBP fbp.bin
nvflash --resume --read FCG fcg.bin
nvflash --resume --read UDA uda.img

#!/bin/sh

PATH=$(pwd)/../../../tools/nvflash4:${PATH}

nvflash \
  --bct E1780_Hynix_2GB_H5TC4G63AFR_RDA_924Mhz.cfg \
  --setbct \
  --configfile tn8_android_fastboot_nvtboot_dtb_emmc_full.cfg \
  --create \
  --bl bootloader.bin \
  --odmdata 0x00098000 \
  --nct NCT_ardbeg.txt \
  --go

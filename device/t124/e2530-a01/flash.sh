#!/bin/sh

PATH=$(pwd)/../../../tools/nvflash4:${PATH}

nvflash \
  --bct E2530_A00_Hynix_4GB_H5TQ4G83AFR_TEC_1056Mhz_r509_v6.cfg \
  --setbct \
  --configfile tn8_android_fastboot_nvtboot_dtb_emmc_full.cfg \
  --create \
  --bl bootloader.bin \
  --odmdata 0x0069C000 \
  --nct NCT_loki_ffd_sku0_a1.txt \
  --go

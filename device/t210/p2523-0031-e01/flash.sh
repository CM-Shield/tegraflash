#!/bin/sh

PATH=$(pwd)/../../../tools/tegraflash:${PATH}

tegraflash.py \
  --bl cboot.bin \
  --odmdata 0x694000 \
  --bct P2530_E01_3GB_Samsung_lpddr4_204Mhz_P972_v2.cfg \
  --bldtb tegra210-loki-e-p2530-0031-e01-00.dtb \
  --applet nvtboot_recovery.bin \
  --nct p2523-0031-e01.bin \
  --cmd "flash;reboot" \
  --cfg flash_t210_android_sdmmc_fb.xml \
  --chip 0x21

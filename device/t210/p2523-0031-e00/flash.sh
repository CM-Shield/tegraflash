#!/bin/sh

PATH=$(pwd)/../../../tools/tegraflash:${PATH}

tegraflash.py \
  --bl cboot.bin \
  --odmdata 0x694000 \
  --bldtb tegra210-loki-e-p2530-0031-e00-00.dtb \
  --applet nvtboot_recovery.bin \
  --nct p2523-0031-e00.bin \
  --cmd "flash;reboot" \
  --cfg flash_t210_android_sdmmc_fb.xml \
  --chip 0x21

#!/bin/bash

PATH=${PWD}/../../../tools/nvblob:${PWD}/../../../tools/tegraflash:${PATH}

export ANDROID_BUILD_TOP=..
export OUT=${PWD}

BUP_generator.py -t bmp -e \
  "../../../bmp/nvidia1080.bmp nvidia 1080; \
   ../../../bmp/verity_orange_continue_1080.bmp verity_orange_continue 1080; \
   ../../../bmp/verity_orange_pause_1080.bmp verity_orange_pause 1080; \
   ../../../bmp/verity_red_continue_1080.bmp verity_red_continue 1080; \
   ../../../bmp/verity_red_pause_1080.bmp verity_red_pause 1080; \
   ../../../bmp/verity_yellow_continue_1080.bmp verity_yellow_continue 1080; \
   ../../../bmp/verity_yellow_pause_1080.bmp verity_yellow_pause 1080"

mkdir blob_signed
pushd blob_signed

ln -s ../*.bin .
ln -s ../*.img .
ln -s ../*.dtb .
ln -s ../*.dat .
ln -s ../flash_android_t210_spi_sd_p3448.xml flash_android_t210_spi_sd_p3448.xml.tmp
ln -s ../P3448_A00_4GB_Micron_4GB_lpddr4_204Mhz_P987.cfg .

tegraparser --pt flash_android_t210_spi_sd_p3448.xml.tmp
tegrahost --chip 0x21 --partitionlayout flash_android_t210_spi_sd_p3448.xml.bin --list images_list.xml
tegrasign --key None --list images_list.xml --pubkeyhash pub_key.key

tegrabct --bct P3448_A00_4GB_Micron_4GB_lpddr4_204Mhz_P987.cfg --chip 0x21
tegrabct --bct P3448_A00_4GB_Micron_4GB_lpddr4_204Mhz_P987.bct --chip 0x21 --updatedevparam flash_android_t210_spi_sd_p3448.xml.bin
tegrabct --bct P3448_A00_4GB_Micron_4GB_lpddr4_204Mhz_P987.bct --chip 0x21 --updateblinfo flash_android_t210_spi_sd_p3448.xml.bin --updatesig images_list_signed.xml
tegraparser --pt flash_android_t210_spi_sd_p3448.xml.bin --chip 0x21 --updatecustinfo P3448_A00_4GB_Micron_4GB_lpddr4_204Mhz_P987.bct
tegraparser --nct p3448-0000-a02.bin --chip 0x21 --updatecustinfo P3448_A00_4GB_Micron_4GB_lpddr4_204Mhz_P987.bct
tegrabct --bct P3448_A00_4GB_Micron_4GB_lpddr4_204Mhz_P987.bct --chip 0x21 --updatefields "Odmdata =0x94000"
tegrabct --bct P3448_A00_4GB_Micron_4GB_lpddr4_204Mhz_P987.bct --chip 0x21 --listbct bct_list.xml
tegrasign --key None --list bct_list.xml --pubkeyhash pub_key.key
tegrabct --bct P3448_A00_4GB_Micron_4GB_lpddr4_204Mhz_P987.bct --chip 0x21 --updatesig bct_list_signed.xml
tegrahost --chip 0x21 --partitionlayout flash_android_t210_spi_sd_p3448.xml.bin --updatesig images_list_signed.xml
tegrabct --bct P3448_A00_4GB_Micron_4GB_lpddr4_204Mhz_P987.bct --chip 0x21 --updatebfsinfo flash_android_t210_spi_sd_p3448.xml.bin

popd

dd if=/dev/zero of=bpmp_zeroes.bin bs=1024 count=2048

cp tegra210-p3448-0000-p3449-0000-a02-android-devkit.dtb temp.dtb
cp boot.img boot.tmp
cp twrp.img twrp.tmp
tegraflash.py \
  --bct P3448_A00_4GB_Micron_4GB_lpddr4_204Mhz_P987.cfg \
  --applet nvtboot_recovery.bin \
  --cmd "sign" \
  --cfg sign.xml \
  --chip 0x21
rm -rf temp.dtb boot.tmp twrp.tmp

BUP_generator.py -t update -e \
  "blob_signed/nvtboot.bin.encrypt NVC 2 0 common; \
   blob_signed/nvtboot.bin.encrypt NVC-B 2 0 common; \
   bpmp_zeroes.bin BPF 2 0 common; \
   blob_signed/nvtboot_cpu.bin.encrypt TBC 2 0 common; \
   blob_signed/nvtboot_cpu.bin.encrypt TBC-B 2 0 common; \
   blob_signed/cboot.bin.encrypt EBT 2 0 common; \
   blob_signed/cboot.bin.encrypt RBL 2 0 common; \
   blob_signed/warmboot.bin.encrypt WB0 2 0 common; \
   blob_signed/tos.img.encrypt TOS 2 0 common; \
   rp4.blob RP4 2 0 common; \
   signed/temp.dtb.encrypt DTB 2 0 common; \
   signed/temp.dtb.encrypt RP1 2 0 common; \
   blob_signed/P3448_A00_4GB_Micron_4GB_lpddr4_204Mhz_P987.bct BCT 2 0 common"

rm -rf blob_signed bpmp_zeroes.bin signed

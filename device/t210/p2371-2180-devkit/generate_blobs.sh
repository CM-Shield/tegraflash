#!/bin/bash

PATH=${PWD}/../../../tools/nvblob:${PWD}/../../../tools/tegraflash:${PATH}

export ANDROID_BUILD_TOP=..
export OUT=${PWD}

BUP_generator.py -t bmp -e \
  "../../../bmp/nvidia1080.bmp nvidia 1080; \
   ../../../bmp/verity_orange_continue_1080.bmp verity_orange_continue 1080; \
   ../../../bmp/verity_orange_pause_1080.bmp verity_orange_pause 1080; \
   ../../../bmp/verity_red_continue_1080.bmp verity_red_continue 1080; \
   ../../../bmp/verity_red_pause_1080.bmp verity_red_pause 1080; \
   ../../../bmp/verity_yellow_continue_1080.bmp verity_yellow_continue 1080; \
   ../../../bmp/verity_yellow_pause_1080.bmp verity_yellow_pause 1080"

mkdir blob_signed
pushd blob_signed

ln -s ../*.bin .
ln -s ../*.img .
ln -s ../*.dtb .
ln -s ../*.dat .
ln -s ../flash_t210_android_sdmmc_fb.xml flash_t210_android_sdmmc_fb.xml.tmp
ln -s ../P2180_A00_LP4_DSC_204Mhz.cfg .

tegraparser --pt flash_t210_android_sdmmc_fb.xml.tmp
tegrahost --chip 0x21 --partitionlayout flash_t210_android_sdmmc_fb.xml.bin --list images_list.xml
tegrasign --key None --list images_list.xml --pubkeyhash pub_key.key

tegrabct --bct P2180_A00_LP4_DSC_204Mhz.cfg --chip 0x21
tegrabct --bct P2180_A00_LP4_DSC_204Mhz.bct --chip 0x21 --updatedevparam flash_t210_android_sdmmc_fb.xml.bin
tegrabct --bct P2180_A00_LP4_DSC_204Mhz.bct --chip 0x21 --updateblinfo flash_t210_android_sdmmc_fb.xml.bin --updatesig images_list_signed.xml
tegraparser --pt flash_t210_android_sdmmc_fb.xml.bin --chip 0x21 --updatecustinfo P2180_A00_LP4_DSC_204Mhz.bct
tegraparser --nct p2371-2180-devkit.bin --chip 0x21 --updatecustinfo P2180_A00_LP4_DSC_204Mhz.bct
tegrabct --bct P2180_A00_LP4_DSC_204Mhz.bct --chip 0x21 --updatefields "Odmdata =0x94000"
tegrabct --bct P2180_A00_LP4_DSC_204Mhz.bct --chip 0x21 --listbct bct_list.xml
tegrasign --key None --list bct_list.xml --pubkeyhash pub_key.key
tegrabct --bct P2180_A00_LP4_DSC_204Mhz.bct --chip 0x21 --updatesig bct_list_signed.xml
tegrahost --chip 0x21 --partitionlayout flash_t210_android_sdmmc_fb.xml.bin --updatesig images_list_signed.xml
tegrabct --bct P2180_A00_LP4_DSC_204Mhz.bct --chip 0x21 --updatebfsinfo flash_t210_android_sdmmc_fb.xml.bin

popd

dd if=/dev/zero of=bpmp_zeroes.bin bs=1024 count=2048

BUP_generator.py -t update -e \
  "blob_signed/nvtboot.bin.encrypt NVC 2 0 common; \
   blob_signed/nvtboot.bin.encrypt NVC-B 2 0 common; \
   bpmp_zeroes.bin BPF 2 0 common; \
   blob_signed/nvtboot_cpu.bin.encrypt TBC 2 0 common; \
   blob_signed/nvtboot_cpu.bin.encrypt TBC-B 2 0 common; \
   blob_signed/cboot.bin.encrypt EBT 2 0 common; \
   blob_signed/cboot.bin.encrypt RBL 2 0 common; \
   blob_signed/warmboot.bin.encrypt WB0 2 0 common; \
   blob_signed/tos.img.encrypt TOS 2 0 common; \
   rp4.blob RP4 2 0 common; \
   tegra210-jetson-tx1-p2597-2180-a01-android-devkit.dtb DTB 2 0 common; \
   tegra210-jetson-tx1-p2597-2180-a01-android-devkit.dtb RP1 2 0 common; \
   blob_signed/P2180_A00_LP4_DSC_204Mhz.bct BCT 2 0 common"

rm -rf blob_signed bpmp_zeroes.bin
